#############################################################
#Code taken from the internet for learning purposes, not mine
#############################################################

import matplotlib.pylab as plt
import numpy as np

'''
#Block to simulate the sigmoid function to squeeze values so 0 <= x <= 1 

#arange returns an array of evenly spaced valus between -8 and 8 with a step of 0.1
x = np.arange(-8, 8, 0.1) 
#plugs array x into sigmoid function 
f = 1 / (1 + np.exp(-x))
#plot the function
plt.plot(x, f)
plt.xlabel('x')
plt.ylabel('f(x)')
plt.show()


#Block to show affect of weights on sigmoid function
x = np.arange(-8, 8, 0.1)
#w == weights
w1 = 0.5
w2 = 1.0
w3 = 2.0
l1 = 'w = 0.5'
l2 = 'w = 1.0'
l3 = 'w = 2.0'
#loop to plot three lines using tuples
for w, l in [(w1, l1), (w2, l2), (w3, l3)]:
	f = 1 / (1 + np.exp(-x * w))
	plt.plot(x, f, label=l)
plt.xlabel('x')
plt.ylabel('h_w(x)')
#loc = location of legend
plt.legend(loc = 2)
plt.show()


x = np.arange(-8, 8, 0.1)
w = 5.0
b1 = -8.0
b2 = 0.0
b3 = 8.0
l1 = 'b = -8.0'
l2 = 'b = 0.0'
l3 = 'b = 8.0'

for b, l in [(b1, l1), (b2, l2), (b3, l3)]:
	f = 1 / (1 + np.exp(-(x * w + b)))
	plt.plot(x, f, label = l)
plt.xlabel('x')
plt.ylabel('h_wb(x)')
plt.legend(loc=2)
plt.show()
'''

#Example array for the weights of layer1
w1 = np.array([[0.2, 0.2, 0.2], [0.4, 0.4, 0.4], [0.6, 0.6, 0.6]])
#zeros returns an array with dimensions 1:3 filled with zeroes
w2 = np.zeros((1,3))
#sets all the columns(layers) in the 0 row to the values given
w2[0,:] = np.array([0.5, 0.5, 0.5])
#creates bias for the first two layers
b1 = np.array([0.8, 0.8, 0.8])
b2 = np.array([0.2])

def sigmoid(x):
	return 1 / (1 + np.exp(-x))

def simple_looped_nn_calc(n_layers, x, w, b):
	#for each layer in the number of layers - 1
	for layer in range(n_layers - 1):
		#if it is the first layer, the input will be the x input vecotor
		if layer == 0:
			node_in = x
		#if it is not the first layer, the input will be the output of the previous layer
		else:
			node_in = h
	# setup input array for nodes in layer (l + 1), .shape[0] returns the number of rows 
	h = np.zeros((w[layer].shape[0],))
	#loop through the rows of the weight array, .shape[1] returns the number of columns
	for row in range(w[layer].shape[1]):
		#initialize sum inside of the sigmoid function
		f_sum = 0

		for column in range(w[layer].shape[1]):
			# for each row, column in the current weight layer, multiply by the input value of the
			#of the node in the corrent column
			f_sum += w[layer][row][column] * node_in[column] 
		#add bias
		f_sum += b[layer][row]
		#use the sigmoid function to calculate the ith output
		h[row] = sigmoid(f_sum)
	return h






